let button      = document.querySelector('#button')
let name        = document.querySelector('#name')
let height      = document.querySelector('#height')
let wieght      = document.querySelector('#weight')
let birthDate   = document.querySelector('#birth-date')

function getInfo() 
	{
		let random = Math.floor( (Math.random() * 88)  + 1)
		
		let apiURL = 'http://swapi.co/api/people/' + random

		axios.get(apiURL).then(function(response) {
			Update(response.data)
		}).catch(e => {
			infoWithError()
		})
	}

function Update(data) 
	{
		name.innerText        = data.name
		height.innerText      = `Height- ${data.height}`
		weight.innerText      = `Weight- ${data.weight}`
		birthDate.innerText   = `Birth Date- ${data.birth_date}`
	}

function infoWithError() 
	{
		name.innerText        = 'Error while loading a person data'
		height.innerText      = ' '
		weight.innerText      = ' '
		birthDate.innerText   = ' '
	}

button.addEventListener('click', getInfo)
